package com.minh.hw1.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryDto {
    private String query;
    private Boolean force;
}
