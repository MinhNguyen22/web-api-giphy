package com.minh.hw1.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GifDto implements Serializable {
    private Object data;
}
