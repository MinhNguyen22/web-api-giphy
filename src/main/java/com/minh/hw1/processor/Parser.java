package com.minh.hw1.processor;

import com.minh.hw1.dto.CacheDto;
import com.minh.hw1.dto.HistoryDto;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class Parser {
    public CacheDto[] parseFullCache(Map<String, File[]> queriedCache) {

        CacheDto[] cache = new CacheDto[queriedCache.size()];
        String[] buffArr = new String[0];
        boolean first = true;
        int counter = 0;
        int cacheCounter = 0;
        int length = 0;
        int length1 = 0;

        for (Map.Entry<String, File[]> path : queriedCache.entrySet()) {
            for (File file : queriedCache.get(path.getKey())) {
                if (file.listFiles() != null)
                    length += file.listFiles().length;
            }
        }
        buffArr = new String[length];
        // very sad parser... but unfortunately i have no time to refactor it :(
        for (Map.Entry<String, File[]> path : queriedCache.entrySet()) {
            String[] array = new String[path.getValue().length];
            for (File file : queriedCache.get(path.getKey())) {
                if (file.listFiles() != null) {
                    System.out.println("Included for working");
                    array = new String[file.listFiles().length];
                    for (File gif : file.listFiles()) {
                        array[counter] = gif.getAbsolutePath();
                        counter++;
                    }
                    counter = 0;
                } else { // crutch ...
                    array[counter] = file.getAbsolutePath();
                    counter++;

                }
                if (buffArr.length != 0) {
                    if (first) {
                        System.arraycopy(array, 0, buffArr, 0, array.length);
                        first = false;
                    } else {
                        System.arraycopy(array, 0, buffArr, length1, array.length);
                    }
                    length1 += array.length;
                    array = buffArr;

                }
            }
            counter = 0;
            cache[cacheCounter] = new CacheDto();
            cache[cacheCounter].setQuery(path.getKey());
            cache[cacheCounter].setGifs(array);
            cacheCounter++;
        }

        return cache;
    }

    public String[] parseOnlyFiles(Map<String, File[]> queriedCache) {
        var cacheDto = parseFullCache(queriedCache);
        String[] res;
        int length = 0;
        int length1;
        boolean first = true;
        for (CacheDto cache : cacheDto) {
            length += cache.getGifs().length;
        }
        res = new String[length];
        for (CacheDto cache : cacheDto) {
            length1 = cache.getGifs().length;
            if (first) {
                System.arraycopy(cache.getGifs(), 0, res, 0, cache.getGifs().length);
                first = false;
            } else {
                System.arraycopy(cache.getGifs(), 0, res, length1, cache.getGifs().length);
            }
        }
        return res;
    }

    public List<HistoryDto> historyParser(File file) {
        var historyList = new ArrayList<HistoryDto>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                var history = new HistoryDto();
                String[] result = line.split(",");
                history.setDate(LocalDate.parse(result[0]));
                history.setQuery(result[1]);
                history.setGif(result[2]);
                historyList.add(history);
            }
        } catch (IOException ex) {
            System.out.println("Impossible to read file");
        }

        return historyList;
    }

    public HistoryDto[] parseHistory(File file) {
        var resultList = historyParser(file);
        var resultArray = new HistoryDto[resultList.size()];
        return resultList.toArray(resultArray);
    }
}