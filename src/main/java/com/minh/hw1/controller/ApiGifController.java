package com.minh.hw1.controller;

import com.minh.hw1.dto.CacheDto;
import com.minh.hw1.dto.QueryDto;
import com.minh.hw1.entity.Cache;
import com.minh.hw1.processor.FileSystemProcessor;
import com.minh.hw1.processor.Parser;
import com.minh.hw1.service.GifService;
import com.minh.hw1.util.UserUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ApiGifController {

    GifService giphyService;
    FileSystemProcessor fileSystemProcessor;
    Cache cache;
    UserUtil userUtil;
    Parser parser;
    CacheDto cacheDto;

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public ApiGifController(GifService giphyServ,
                             FileSystemProcessor fileProcessor,
                             Cache cache,
                             UserUtil userUtil,
                             Parser parser,
                             CacheDto cacheDto){
        this.giphyService = giphyServ;
        this.fileSystemProcessor = fileProcessor;
        this.cache = cache;
        this.userUtil = userUtil;
        this.parser = parser;
        this.cacheDto = cacheDto;
    }

    @GetMapping("/gifs")
    public ResponseEntity<?> getAllGifs() {

        logger.info("GET request, CLASS: GeneralController, METHOD: getAllGifs(...)");

        var arrayOfAllGifs = parser.parseOnlyFiles(fileSystemProcessor.getFullCache(null));
        return new ResponseEntity<>(arrayOfAllGifs, HttpStatus.OK);
    }

    @GetMapping("/cache")
    public ResponseEntity<?> getDiskCache(String query) {

        logger.info("GET request, CLASS: GeneralController, METHOD: getDiskCache(...), QUERY: " + query);

        var fullCache = parser.parseFullCache(fileSystemProcessor.getFullCache(query));

        return new ResponseEntity<>(fullCache, HttpStatus.OK);
    }

    @DeleteMapping("/cache")
    public ResponseEntity<?> clearDiskCache() {
        logger.info("DELETE request, CLASS: GeneralController, METHOD: clearDickCache(...)");

        fileSystemProcessor.clearCache();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/cache/generate")
    public ResponseEntity<?> generateGif(@RequestBody QueryDto query) {
        logger.info("POST request, CLASS: GeneralController, METHOD: generateGif(...), QUERY:" + query.getQuery() + ", FORCE: " + query.getForce());

        var giphy = giphyService.searchGif(null, query);
        fileSystemProcessor.downloadGifByUrl(giphy);

        CacheDto cache = parser.parseFullCache(fileSystemProcessor.getFullCache(query.getQuery()))[0];

        return new ResponseEntity<>(cache, HttpStatus.OK);
    }
}
