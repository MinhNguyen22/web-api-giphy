package com.minh.hw1.entity;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Component
@Getter
public class Cache {
    private Map<String, Map<String, ArrayList<String>>> map;
    private static Cache uniqInstance;

    private Cache() {
        map = new HashMap<>();
    }

    public static Cache getInstance() {
        if (uniqInstance == null) {
            uniqInstance = new Cache();
        }

        return uniqInstance;
    }

    public void updateCache(String user_id, String query, String gifId) {

        var tempList = new ArrayList<String>();

        if (this.map.get(user_id) != null) {
            if (this.map.get(user_id).get(query) != null) {
                tempList = this.map.get(user_id).get(query);
                tempList.add(tempList.size(), gifId);
            } else {
                tempList.add(gifId);
            }
            this.map.get(user_id).put(query, tempList);
        } else {
            var userMap = new HashMap<String, ArrayList<String>>();
            tempList.add(gifId);
            userMap.put(query, tempList);
            this.map.put(user_id, userMap);
        }

    }

    public String getGif(String user_id, String query) {
        if (this.map.get(user_id) != null) {
            if (this.map.get(user_id).get(query) != null) {
                var queriedList = new ArrayList<String>();
                queriedList = this.map.get(user_id).get(query);
                return queriedList.get(new Random().nextInt(queriedList.size()));
            }
        }
        return null;
    }

    public void resetUser(String user_id) {
        this.map.remove(user_id);
    }

}

