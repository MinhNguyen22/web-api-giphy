package com.minh.hw1.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
public class Gif {

    private String url;
    private String id;
    private String query;
}

