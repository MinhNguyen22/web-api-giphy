package com.minh.hw1.service;

import com.minh.hw1.dto.GifDto;
import com.minh.hw1.dto.QueryDto;
import com.minh.hw1.entity.Gif;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class GifService {

    Environment environment;

    @Autowired
    public GifService(Environment environment){
        this.environment = environment;
    }

    public Gif searchGif(String user_id, QueryDto query){
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(environment.getProperty("giphy.search.url"))
                .queryParam("api_key", environment.getProperty("giphy.api.key"))
                .queryParam("tag", query.getQuery())
                .queryParam("random_id", user_id);

        GifDto gifFile = restTemplate.getForObject(builder.toUriString(), GifDto.class);

        JSONObject json = new JSONObject(gifFile);
        json = json.getJSONObject("data");

        Gif gifEntity = new Gif();
        gifEntity.setId(json.getString("id"));

        StringBuilder url = new StringBuilder(json.getJSONObject("images").getJSONObject("downsized").getString("url"));
        url.replace(8, 14, "i");

        gifEntity.setUrl(url.toString());
        gifEntity.setQuery(query.getQuery());

        return gifEntity;
    }
}
